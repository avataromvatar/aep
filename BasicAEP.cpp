/*
 * BasicAEP.cpp
 *
 *  Created on: 21 нояб. 2019 г.
 *      Author: avatar
 */

#include "BasicAEP.hpp"

namespace nAEP
{

  BasicAEP::BasicAEP (const char *info)
  {
    m_info = info;
  }

  BasicAEP::BasicAEP (const char *info, FunNotify fun, uint16_t id)
  {
    m_info = info;
    m_fun = fun;
    m_id = id;
  }

  BasicAEP::~BasicAEP ()
  {
    // TODO Auto-generated destructor stub
  }

  uint16_t BasicAEP::setRouter (IRouterForUsers *router)
  {
    m_router = router;
    return 0;
  }

  void BasicAEP::notify (uint16_t id, const uint8_t *data, uint16_t len)
  {
    if(m_fun)
      m_fun(id,data,len);
  }

  uint8_t BasicAEP::in_demand ()
  {
    m_you_are_in_demand = 1;
    return 0;
  }

  uint8_t BasicAEP::unclaimed ()
  {
    m_you_are_in_demand = 0;
    return 0;
  }

  const char* BasicAEP::getInfo ()
  {
    return m_info;
  }

  uint16_t BasicAEP::getID ()
  {
    return m_id;
  }

  void BasicAEP::send ( const uint8_t *data, uint16_t len)
  {
    if(m_router)
      m_router->send(m_id,data,len);
  }

  uint8_t BasicAEP::is_in_demand ()
  {
    return m_you_are_in_demand;
  }

} /* namespace nAEP */
