/*
 * AEPRouter.cpp
 *
 *  Created on: 28 окт. 2019 г.
 *      Author: avatar
 */

#include "AEPRouter.hpp"

namespace nAEP
{

  AEPRouter::AEPRouter ()
  {
    // TODO Auto-generated constructor stub

  }

  AEPRouter::~AEPRouter ()
  {
    // TODO Auto-generated destructor stub
  }

  uint8_t AEPRouter::send (uint16_t id, const uint8_t *data, uint16_t len)
  {
    auto it = forward_rules.find (id);
    if (it != forward_rules.end ())
      {
	uint32_t len1 = it->second.size ();
	for (uint32_t i = 0; i < len1; i++)
	  {
	    auto it1 = user.find (it->second[i]);
	    if (it1 != user.end ())
	      {
		it1->second.user->notify (id, data, len);
	      }
	  }
      }
    else
      return 1;

    return 0;
  }
   IRouterUserInfo* AEPRouter::getUserEPNumNext(uint16_t id)
   {

     for(uint16_t i= GET_EP_NUM_FROM_AEP_ID(id)+1;i<EP_NUM_MAX;i++)
         {
       auto it=user.find(SET_AEP_ID(i,(uint8_t)GET_TYPE_FROM_AEP_ID(id)));
           if(it != user.end())
     	return it->second.user;
         }
     return 0;
   }
   uint16_t AEPRouter::getFreeEPNum(eTypeLink type)
  {
    for(uint16_t i= 1;i<EP_NUM_MAX;i++)
    {
      if(user.find(SET_AEP_ID(i,(uint8_t)type)) == user.end())
	return SET_AEP_ID(i,(uint8_t)type);
    }
    return 0;
  }

  int AEPRouter::addLink (uint16_t id1, uint16_t id2, eTypeLink type_link)
  {
//    if (type_link == ONE_DIRECT )
//    {
    addTo (forward_rules, id1, id2);
    addTo (back_rules, id2, id1);
//    }
//    else
    if (type_link == BIDIRECTIONAL)
      {
	addTo (forward_rules, id2, id1);
	addTo (back_rules, id1, id2);
      }
    return 0;
  }

  int AEPRouter::removeLink (uint16_t id1, uint16_t id2, eTypeLink type_link)
  {
    removeFrom (forward_rules, id1, id2);
    removeFrom (back_rules, id2, id1);
    if (type_link == BIDIRECTIONAL)
      {
	removeFrom (forward_rules, id2, id1);
	removeFrom (back_rules, id1, id2);
      }
    if(forward_rules.find(id1)==forward_rules.end())
      {
	auto it = user.find(id1);
	if(it !=user.end())
	  {
	    it->second.is_unclaimed = 1;
	    it->second.user->unclaimed();
	  }
      }
    return 0;
  }

  int AEPRouter::removeLink (uint16_t id)
  {
    return removeFrom(id);

  }

  int AEPRouter::addUser (IRouterUser *ep)
  {
    if(user.find(ep->getID())==user.end())
      {
	user[ep->getID()].user=ep;
	user[ep->getID()].user->setRouter(this);
	return 0;
      }
    return 1;
  }

  uint8_t AEPRouter::addTo (std::map<uint16_t, std::vector<uint16_t>> &rule, uint16_t id1, uint16_t id2)
  {
    auto it = rule.find (id1);
    if (it != rule.end ())
      {
	uint32_t len = it->second.size ();
	for (uint32_t i = 0; i < len; i++)
	  {
	    if (it->second[i] == id2)
	      {
		len = 0;
	      }

	  }
	if (len != 0)
	  {
	    it->second.push_back (id2);
	  }
	else
	  return 1; //такое правило уже испольтзуется

      }
    else
      {
	rule[id1].push_back (id2);
      }
    auto it1 = user.find(id1);
    if(it1 !=user.end())
      {
	if(it1->second.is_unclaimed)
	  {
	    it1->second.is_unclaimed = 0;
	    it1->second.user->in_demand();
	  }
      }
    return 0;
  }

  uint8_t AEPRouter::removeFrom (std::map<uint16_t, std::vector<uint16_t> > &rule, uint16_t id1, uint16_t id2)
  {
    auto it = rule.find (id1);
    if (it != rule.end ())
      {
	uint32_t len = it->second.size ();
	for (uint32_t i = 0; i < len; i++)
	  {
	    if (it->second[i] == id2)
	      {
		it->second.erase (it->second.begin () + i);
		return 0;
	      }

	  }
      }
    else
      return 1;

    return 0;
  }

  uint8_t AEPRouter::removeFrom (uint16_t id)
  {
    /*----- Удаляем из правил передачи -----*/
    auto it = forward_rules.find (id);
    if (it != forward_rules.end ())
      {
	forward_rules.erase (id);
      }
    /*----- Удаляем из правил передачи других устройств -----*/
    auto it1 = back_rules.find (id); //получаем список всех ттех кто передовал этому устройству
    if (it1 != back_rules.end ())
      {
	uint32_t len = it1->second.size ();
	for (uint32_t i = 0; i < len; i++)
	  {
	    //ищем передастов к нашему удаленному устройству
	    it = forward_rules.find (it1->second[i]);
	    if (it != forward_rules.end ())
	      {
		uint32_t len1 = it1->second.size ();
		for (uint32_t i1 = 0; i1 < len1; i1++)
		  {
		    if (it->second[i1] == id)
		      {
			it->second.erase (it->second.begin () + i1);
			break;
		      }

		  }
	      }

	  }
      }
    return 0;
  }

} /* namespace nAEP */
