/*
 * BasicAEP.hpp
 *
 *  Created on: 21 нояб. 2019 г.
 *      Author: avatar
 */

#ifndef BASICAEP_HPP_
#define BASICAEP_HPP_

#include "AEP_Common.hpp"
#include "functional"
namespace nAEP
{

  class BasicAEP : public IRouterUser
  {
    public:
      typedef std::function<void (uint16_t id, const uint8_t *data, uint16_t len)> FunNotify;

      BasicAEP (const char *info);
      BasicAEP (const char *info, FunNotify fun, uint16_t id);
      virtual ~BasicAEP ();

      void send( const uint8_t *data, uint16_t len);

      /*----- IRouterUser -----*/
      uint16_t setRouter (IRouterForUsers *router);
      void notify (uint16_t id, const uint8_t *data, uint16_t len);
      uint8_t in_demand (); //ты востребована можешь слать данные
      uint8_t unclaimed (); //ты невостребована можешь не пытатся слать данные
      /*----- IRouterUserInfo -----*/
      const char* getInfo ();
      uint16_t getID ();
      uint8_t is_in_demand ();
    protected:
      uint16_t m_id=0;
      const char *m_info;
      FunNotify m_fun=0;
      uint8_t m_you_are_in_demand=0;
      IRouterForUsers *m_router=0;
    private:


  };

} /* namespace nAEP */

#endif /* BASICAEP_HPP_ */
