/*
 * AEP_Common.hpp
 *
 *  Created on: 25 окт. 2019 г.
 *      Author: avatar
 *
 *
 *
 *
 *
 *
// *      EP TYPE SYS служать для обмена служебной информации и настройке правил конечных точек
// *      Обмен сообщениями между ними идет по тех. Запрос-Ответ
// *      Каждая EP SYS SLAVE ассоциируется с EP SYS MASTER
// *      Тоесть когда в AEBus приходит запрос от Клиента с AEP_TYPE_SYS_MASTER,0 он автоматом будет перенаправлен
// *      на EP SYS SLAVE,0.
// *
// *      Перед пладотворной работой устройства должны познакомится через EP SYS
// *
// *	Сообщение хранится на стороне TransportTerminal с id_EP отправителя если система работает
// *	в режиме Запрос Ответ то по этому id_EP отправится ответ из TransportTerminal
// *
 */

#ifndef CORE_AEP_AEP_COMMON_HPP_
#define CORE_AEP_AEP_COMMON_HPP_

#include "stdint.h"
#include "vector"

namespace nAEP
{

#define EP_NUM_MAX (0xFFF)
#define GET_TYPE_FROM_AEP_ID(id) (id&0x000F)
#define GET_EP_NUM_FROM_AEP_ID(id) ((id>>4)&EP_NUM_MAX)
#define SET_AEP_ID(ep_num,type) (((ep_num&EP_NUM_MAX)<<4)|(type&0xF))

  /// 4 bits
  enum eAEPType : uint8_t
  {
    AEP_TYPE_SYS = 0, // send request - receive response  msg
    AEP_TYPE_PUBLISHER = 0x01, // send msg
    AEP_TYPE_OBSERVABLE = 0x02, // send msg
    AEP_TYPE_STREAM = 0x03, // send msg
	AEP_TYPE_MODBUSDATA_SERVER = 0x04, // receive request - send response  msg
	AEP_TYPE_TERMINAL = 0x05, // send/recive msg
	AEP_TYPE_FUNCTION_1 = 0x06, // send/recive msg
	AEP_TYPE_FUNCTION_2 = 0x07, // send/recive msg
	AEP_TYPE_LOCAL_FUNCTION = 0x0E,
    AEP_TYPE_LOCAL_TT = 0x0F
  };

#pragma pack(push,2)
  struct sOBSERVABLE_Protocol
  {
    uint16_t size_cell:3;
    uint16_t num_cell:13;
    uint32_t data;
  };
#pragma pack ( pop)
  enum eAEP_ERROR : uint8_t
  {
    AEP_ERROR_NO, AEP_ERROR_YES
  };

  /**
   * enum TransportTerminal
   */
  enum eTTType : uint32_t
  {
    TTTYPE_TCP, //!< TTTYPE_TCP
    TTTYPE_UDP, //!< TTTYPE_UDP
    TTTYPE_PIPE, //!< TTTYPE_PIPE
    TTTYPE_WEBSOCKET,
    TTTYPE_UART,
    TTTYPE_CAN,
    TTTYPE_SPI,
    TTTYPE_IPC //Inter Process Communications
  };

  struct sAEPFrameHeader
  {
    uint32_t request_flag :1; //1 IN Can lower prior
    uint32_t type :4;
    uint32_t EP :12; //
    uint32_t count :8;
    uint32_t data_len :7; //CAN transport data_len = max 8 byte

  };
  union uAEPFrameHead
  {
    sAEPFrameHeader bf;
    uint32_t val;
  };
  struct sAEPFrame
  {

    uAEPFrameHead head;
    std::vector<uint8_t> data;
  };

//  struct sAEPMsg
//  {
//    union uState
//    {
//      struct State
//      {
//	uint8_t is_use :1;
//	uint8_t is_empty :1;
//	uint8_t is_request :1;
//	uint8_t send :1;
//	uint8_t wait_response :1;
//	uint8_t have_response :1;
//	uint8_t reserv :2;
//      } bits;
//      uint8_t val;
//    } state;
//    uint8_t type_sender = 0;
//    uint8_t EP_sender = 0;
//    uint16_t len = 0;
//    const uint8_t *data;
////    std::vector<uint8_t> data;
//    sAEPMsg *response;
//  };

  class IRouterForUsers
  {
  public:
    virtual uint8_t send (uint16_t id, const uint8_t *data, uint16_t len)=0;
  };

  class IRouterUserInfo
      {
      public:
      virtual const char * getInfo()=0;
      virtual uint16_t getID()=0;
      virtual uint8_t is_in_demand()=0;
      };
  class IRouterUser :public IRouterUserInfo
    {
    public:
    virtual uint16_t setRouter(IRouterForUsers * router)=0;
    virtual void notify(uint16_t id,const uint8_t *data, uint16_t len)=0;
    virtual uint8_t in_demand()=0; //ты востребована можешь слать данные
    virtual uint8_t unclaimed()=0;//ты невостребована можешь не пытатся слать данные
    };

  class IRouter:public IRouterForUsers
  {
  public:
    	enum eTypeLink:uint8_t
    	{
    		ONE_DIRECT,
    		BIDIRECTIONAL
    	};
    	virtual int addLink(uint16_t id1,uint16_t id2,eTypeLink type_link)=0;
    	virtual int removeLink(uint16_t id1,uint16_t id2,eTypeLink type_link)=0;
    	virtual int removeLink(uint16_t id)=0;
    	virtual int addUser(IRouterUser *ep)=0;
    	virtual uint16_t getFreeEPNum(eTypeLink type)=0;
    	virtual IRouterUserInfo* getUserEPNumNext(uint16_t id)=0;
  };
//
////class ITransportTerminal;
//
//class IAEPBusUser
//{
//public:
//	enum eTypeAEPUser:uint16_t
//	{
//		END_POINT,
//		CLIENT
//	};
//	virtual uint16_t getID()=0;
//	virtual uint16_t getTypeUser()=0;
//	virtual void notifyFromEP(const sAEPMsg &recive,const uint16_t &id_EP)=0;
//	virtual void notifyFromClient(const sAEPMsg &recive,const uint16_t &id_client)=0;
//
//
//};
//
//class IAEPBus_Admin
//{
//public:
//	virtual int addAEPBusUser(IAEPBusUser * user)=0;
//	virtual int removeAEPBusUser(IAEPBusUser * user)=0;
//
//};
//class IAEPBus_MasterEP
//{
//public:
//	enum eTypeLink:uint8_t
//	{
//		EP_TO_CLIENT_ONE_DIRECT,
//		CLIENT_TO_EP_ONE_DIRECT,
//		CLIENT_TO_CLIENT_ONE_DIRECT,
//		EP_TO_EP_ONE_DIRECT,
//		EP_TO_CLIENT_BIDIRECTIONAL,
//		CLIENT_TO_EP_BIDIRECTIONAL,
//		CLIENT_TO_CLIENT_BIDIRECTIONAL,
//		EP_TO_EP_BIDIRECTIONAL
//	};
//	virtual int addLink(uint16_t id1,uint16_t id2,eTypeLink type_link)=0;
//	virtual int removeLink(uint16_t id1,uint16_t id2,eTypeLink type_link)=0;
//};
//class IAEPBus_
//{
//public:
//	virtual void send(IAEPBusUser *user,const sAEPMsg &send)=0;
////	virtual void sendToClient(const sAEPMsg &send,const uint16_t &id_target_client);
//
//};
//
//
//
////struct sAEPInTT //End point in transport terminal
////{
////
////	sAEPBuffer rx_buff;
////	uint32_t rx_msg_count;
////	uint32_t rx_error_count;
////	uint32_t rx_byte_count;
////	sAEPBuffer tx_buff;
////	uint32_t tx_msg_count;
////	uint32_t tx_error_count;
////	uint32_t tx_byte_count;
////	uint32_t timeout;
////	uint32_t last_error_rx;
////	uint32_t last_error_tx;
////	IAEndPoint * ep;
////};
//
//
//
//
//
//class ITransportTerminal
//{
//public:
//
//	virtual eAEP_ERROR open(uint32_t eTTType,const char * open)=0;
//	virtual eAEP_ERROR connect(const char * conn)=0;
//
//
////	virtual eAEP_ERROR setBufferRXSize(uint32_t new_size)=0;
////	virtual eAEP_ERROR setBufferTXSize(uint32_t new_size)=0;
////	virtual eAEP_ERROR flushBufferRX(IAEndPoint * ep)=0;
////	virtual eAEP_ERROR flushBufferTX(IAEndPoint * ep)=0;
////	virtual eAEP_ERROR setFrameDataLenLimit(uint8_t limit_byte_to_send)=0;
////	virtual eAEP_ERROR getFrameDataLenLimit()=0;
////	virtual eAEP_ERROR write(uint16_t id_ep_source,const uint32_t &id_target_terminal,const sAEPMsg &send)=0;
////	virtual eAEP_ERROR addEP(IAEndPoint *ep)=0;
////	virtual const char * getInfoTerminal()=0;//from my self
////	virtual const char * getInfoTerminal(uint32_t id_target_terminal)=0;
////	virtual uint8_t read(sAEPMsg *send)=0;
//};

}//namespace AEP{
#endif /* CORE_AEP_AEP_COMMON_HPP_ */
