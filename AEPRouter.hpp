/*
 * AEPRouter.hpp
 *
 *  Created on: 28 окт. 2019 г.
 *      Author: avatar
 */

#ifndef CORE_AEP_AEPROUTER_HPP_
#define CORE_AEP_AEPROUTER_HPP_

#include "AEP_Common.hpp"
#include "map"
#include "vector"
namespace nAEP
{

  /**
   * @brief Класс предназначен для пересылки данных между Конечными точками по установленным правилам
   *  и установку правил для отправки.
   */
  class AEPRouter:public IRouter
  {
  public:
    AEPRouter ();
    virtual ~AEPRouter ();

    /**
     * @brief Отправить данные.
     * @param[in] id
     * @param[in] data
     * @param[in] len
     * @return 0 - успех
     */
    virtual uint8_t send (uint16_t id, const	 uint8_t *data, uint16_t len);

    virtual int addLink(uint16_t id1,uint16_t id2,eTypeLink type_link);
    virtual int removeLink(uint16_t id1,uint16_t id2,eTypeLink type_link);
    virtual int removeLink(uint16_t id);
    virtual int addUser(IRouterUser *ep);
    virtual uint16_t getFreeEPNum(eTypeLink type);
    virtual IRouterUserInfo* getUserEPNumNext(uint16_t id);

  private:
    uint8_t addTo(std::map<uint16_t,std::vector<uint16_t>> &rule,uint16_t id1,uint16_t id2);
    uint8_t removeFrom(std::map<uint16_t,std::vector<uint16_t>> &rule,uint16_t id1,uint16_t id2);
    uint8_t removeFrom(uint16_t id);

    struct sAEPRouterUser
    {
      IRouterUser* user=0;
      uint8_t is_unclaimed=1;
      uint16_t external_id=0;
    };
    /**
     *  когда клиент исчезает его надо убрать из всех связей с помощью forward_rules
     *  мы убераем чтобы мертвый клиент нечего никому не слал,
     *  а с помощью back_rules мы смотрим кто ему что то шлет и по id_sendera убераем id_recivera(клиента) из  forward_rules
     */
    std::map<uint16_t,sAEPRouterUser> user;
    std::map<uint16_t,std::vector<uint16_t>> forward_rules; ///хранит send to -> правила map<id_sender,vector<id_target>>
    std::map<uint16_t,std::vector<uint16_t>> back_rules; ///хранит recive from <- правила map<id_recivera,vector<id_sendera>
  };

} /* namespace nAEP */

#endif /* CORE_AEP_AEPROUTER_HPP_ */
